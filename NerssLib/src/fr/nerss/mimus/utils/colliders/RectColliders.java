package fr.nerss.mimus.utils.colliders;

import fr.nerss.mimus.utils.maths.Vector2f;

public class RectColliders extends Colliders {
	public float x, y, w, h;
	public RectColliders(float x, float y, float w, float h) {
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
	}

	public Vector2f isCollid(Colliders c) {
		if(c instanceof RectColliders) {
			return Colliders.rectToRect(this, (RectColliders) c);
		}
		if(c instanceof CircleColliders) {
			return Colliders.rectToCircle(this, (CircleColliders) c);
		}
		return null;
	}

	public String toString() {
		return "RectColliders [x=" + x + ", y=" + y + ", w=" + w + ", h=" + h + "]";
	}

}
