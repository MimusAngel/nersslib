package fr.nerss.mimus.utils.colliders;

import fr.nerss.mimus.utils.maths.Vector2f;

public abstract class Colliders {
	public abstract Vector2f isCollid(Colliders c);
	
	public static Vector2f circleToCircle(CircleColliders c0, CircleColliders c1) {
		float xx = c1.x - c0.x;
		float yy = c1.y - c0.y;
		float dist = xx*xx + yy*yy;
		float m = c0.r+c1.r;
		if(dist <= m * m) {
			float depthX = xx > 0 ? m - xx : -m - xx;
			float depthY = yy > 0 ? m - yy : -m - yy;
			return new Vector2f(depthX, depthY);
		}
		return null;
	}
	
	public static Vector2f rectToRect(RectColliders r0, RectColliders r1) {
		if(r0.x >= r1.x+r1.w
			|| r0.x+r0.w < r1.x
			|| r0.y >= r1.y+r1.h
			|| r0.y+r0.h < r1.y) {
			return null;
		}
		float xx = (r0.x + r0.w/2) - (r1.x + r1.w/2);
		float yy = (r0.y + r0.h/2) - (r1.y + r1.h/2);
		float mx = r1.w/2 + r0.w/2;
		float my = r1.h/2 + r0.h/2;
		if(Math.abs(xx) >= mx || Math.abs(yy) >= my) {
			return null;
		}
		float depthX = xx > 0 ? mx - xx : -mx - xx;
		float depthY = yy > 0 ? my - yy : -my - yy;
		return new Vector2f(depthX, depthY);
	}
	
	public static Vector2f pointToRect(float x, float y, RectColliders r1) {
		if(x >= r1.x+r1.w
			|| x < r1.x
			|| y >= r1.y+r1.h
			|| y < r1.y) {
				return null;
			}
		float xx = (r1.x + r1.w/2) - x;
		float yy = (r1.y + r1.h/2) - y;
		float mx = r1.w/2;
		float my = r1.h/2;
		if(Math.abs(xx) >= mx || Math.abs(yy) >= my) {
			return null;
		}
		float depthX = xx > 0 ? mx - xx : -mx - xx;
		float depthY = yy > 0 ? my - yy : -my - yy;
		return new Vector2f(depthX, depthY);
	}
	
	public static Vector2f pointToCircle(float x, float y, CircleColliders c1) {
		float xx = c1.x - x;
		float yy = c1.y - y;
		float dist = xx*xx + yy*yy;
		if(dist <= c1.r * c1.r) {
			float depthX = xx > 0 ? c1.r - xx : -c1.r - xx;
			float depthY = yy > 0 ? c1.r - yy : -c1.r - yy;
			return new Vector2f(depthX, depthY);
		}
		return null;
	}
	
	public static Vector2f rectToCircle(RectColliders r0, CircleColliders c1) {
		Vector2f rectToRect = rectToRect(r0, new RectColliders(c1.x-c1.r, c1.y-c1.r, c1.r*2, c1.r*2));
		if(rectToRect == null || rectToRect.isNull()) {
			return null;
		}
		float dist = 0;
		      
	    if (c1.x < r0.x) dist += (r0.x - c1.x) * (r0.x - c1.x);
	    if (c1.x > r0.x+r0.w) dist += (c1.x - (r0.x+r0.w)) * (c1.x - (r0.x+r0.w));
	  
	    if (c1.y < r0.y) dist += (r0.y - c1.y) * (r0.y - c1.y);
	    if (c1.y > r0.y+r0.h) dist += (c1.y - (r0.y+r0.h)) * (c1.y - (r0.y+r0.h));
	    
	    float xx = c1.x - (r0.x + r0.w/2);
		float yy = c1.y - (r0.y + r0.h/2);
	    if(dist <= c1.r * c1.r) {
	    	float mx = r0.w/2 + c1.r;
			float my = r0.h/2 + c1.r;
			if(Math.abs(xx) >= mx || Math.abs(yy) >= my) {
				return null;
			}
			float depthX = xx > 0 ? mx - xx : -mx - xx;
			float depthY = yy > 0 ? my - yy : -my - yy;
			return new Vector2f(depthX, depthY);
	    }
		return null;
	}
}
