package fr.nerss.mimus.utils.colliders;

import fr.nerss.mimus.utils.maths.Vector2f;

public class CircleColliders extends Colliders {
	public float x, y, r;
	public CircleColliders(float x, float y, float r) {
		this.x=x;
		this.y=y;
		this.r=r;
	}

	public Vector2f isCollid(Colliders c) {
		if(c instanceof CircleColliders) {
			return Colliders.circleToCircle(this, (CircleColliders) c);
		}
		if(c instanceof RectColliders) {
			return Colliders.rectToCircle((RectColliders) c, this);
		}
		return null;
	}

	public String toString() {
		return "CircleColliders [x=" + x + ", y=" + y + ", r=" + r + "]";
	}

}
