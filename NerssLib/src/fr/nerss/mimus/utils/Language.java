package fr.nerss.mimus.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Language {
	
	private HashMap<String, String> words;
	
	private String name;
	public Language(String name) {
		this.name = name;
		words = new HashMap<String, String>();
		load();
	}

	private void addWords(String key, String word) {
		words.put(key, word);
	}
	
	private void load() {
		if(!new File("res/langs/"+name+".lang").exists()) {
			System.err.println("Lang: No File Exist res/langs/"+name+".lang");
			return;
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader("res/langs/"+name+".lang"));
			String line = "";
			while((line = reader.readLine()) != null) {
				String data[] = line.split("=");
				if(data.length > 1) {
					addWords(data[0].toLowerCase(), data[1]);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public String getWords(String key) {
		key = key.toLowerCase();
		if(!words.containsKey(key)) {
			return key;
		}
		return words.get(key);
	}
}
