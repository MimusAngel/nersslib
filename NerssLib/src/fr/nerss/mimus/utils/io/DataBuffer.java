package fr.nerss.mimus.utils.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class DataBuffer {
	private int writeID;
	private int readID;
	private byte[] data;
	private int dataSize;
	
	public DataBuffer() {
		this(0xffff);
	}
	public DataBuffer(int size) {
		dataSize = size;
		data = new byte[dataSize];
		writeID = 0;
		readID = 0;
	}
	
	public void flip() {
		byte[] nData = new byte[writeID];
		for(int i = 0; i < writeID; i++) {
			nData[i] = data[i];
		}
		data = nData;
	}
	
	public void clear() {
		if(data != null) {
			for(int i = 0; i < data.length; i++) {
				data[i] = 0;
			}
		}
		data = new byte[dataSize];
		writeID = 0;
		readID = 0;
	}
	
	public static DataBuffer setData(byte[] data) throws Exception {
		DataBuffer db = new DataBuffer();
		for(int i = 0; i<data.length; i++) {
			db.put(data[i]);
		}
		return db;
	}
	
	public byte[] array() {
		return data;
	}
	
	public int size() {
		return array().length;
	}
	
	public void put(byte value) throws Exception {
		if(writeID >= data.length) {
			throw new Exception("Write Overflow..."+writeID+"\n\tMax capacity: "+data.length);
		}
		data[writeID] = value;
		writeID++;
	}
	
	public void put(byte... values) throws Exception {
		for(int i = 0; i < values.length; i++) {
			put(values[i]);
		}
	}
	
	public byte getByte() throws Exception {
		if(readID >= data.length) {
			throw new Exception("Read Overflow..."+writeID+"\n\tMax capacity: "+data.length);
		}
		return data[readID++];
	}
	
	public void put(short w) throws Exception {
		put((byte) (w >> 8));
		put((byte) w);
	}
	
	public short getShort() throws Exception {
		return ByteBuffer.wrap(new byte[] {getByte(), getByte()}).getShort();
	}
	
	public void put(int w) throws Exception {
		put((byte) (w >> 24));
		put((byte) (w >> 16));
		put((byte) (w >> 8));
		put((byte) w);
	}
	
	public int getInt() throws Exception {
		return ByteBuffer.wrap(new byte[] {getByte(), getByte(), getByte(), getByte()}).getInt();
	}

	public void put(long w) throws Exception {
		put((byte) (w >> 56));
		put((byte) (w >> 48));
		put((byte) (w >> 40));
		put((byte) (w >> 32));
		put((byte) (w >> 24));
		put((byte) (w >> 16));
		put((byte) (w >> 8));
		put((byte) w);
	}
	

	public long getLong() throws Exception {
		return ByteBuffer.wrap(new byte[] {getByte(), getByte(), getByte(), getByte(), getByte(), getByte(), getByte(), getByte()}).getLong();
	}
	
	public void put(float w) throws Exception {
		put(Float.floatToIntBits(w));
	}
	
	public float getFloat() throws Exception {
		return ByteBuffer.wrap(new byte[] {getByte(), getByte(), getByte(), getByte()}).getFloat();
	}
	
	public void put(double w) throws Exception {
		put(Double.doubleToLongBits(w));
	}
	
	public double getDouble() throws Exception {
		return ByteBuffer.wrap(new byte[] {getByte(), getByte(), getByte(), getByte(), getByte(), getByte(), getByte(), getByte()}).getDouble();
	}
	
	public void put(String w) throws Exception {
		byte b[] = w.getBytes();
		put(b.length);
		put(b);
	}
	
	public String getString() throws Exception {
		byte[] b = new byte[getInt()];
		for(int i=0; i<b.length; i++) {
			b[i] = getByte();
		}
		return new String(b);
	}
	
	public void write(String path) throws IOException {
		FileOutputStream fos = new FileOutputStream(path);
		fos.write(this.array());
		fos.close();
	}
	
	public void read(String path) throws IOException {
		FileInputStream fis = new FileInputStream(path);
		fis.read(data);
		fis.close();
	}
}
