package fr.nerss.mimus.utils.io.filter;

import java.io.File;
import java.io.FilenameFilter;

public class FileLangFilter implements FilenameFilter {

	@Override
	public boolean accept(File arg0, String arg1) {
		return arg1.toLowerCase().endsWith(".lang");
	}

}
