package fr.nerss.mimus.utils.inputs;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Inputs {
	private static ConcurrentHashMap<Integer, Boolean> down = new ConcurrentHashMap<Integer, Boolean>();
	private static ConcurrentHashMap<Integer, Boolean> up = new ConcurrentHashMap<Integer, Boolean>();
	private static float mouseWheel = 0;
	
	public static void update() {
		mouseWheel = Mouse.getDWheel();
		up.clear();
		for(Entry<Integer, Boolean> input : down.entrySet()) {
			int key = input.getKey();
			if(key == 1100) {
				if(mouseWheel >= 0) up.put(key, true);
			} else if(key == 1101) {
				if(mouseWheel <= 0) up.put(key, true);
			} else if(key >= 1000) {
				if(!Mouse.isButtonDown(key-1000)) up.put(key, true);
			} else {
				if(!Keyboard.isKeyDown(key)) up.put(key, true);
			}
			
		}
		
		for(int i = 0; i < Keyboard.getKeyCount(); i++) {
			if(Keyboard.isKeyDown(i)) {
				if(!down.contains(i)) down.put(i, true);
			} else {
				down.remove(i);
			}
		}
		for(int i = 0; i < Mouse.getButtonCount(); i++) {
			if(Mouse.isButtonDown(i)) {
				if(!down.contains(1000 + i)) down.put(1000 + i, true);
			} else {
				down.remove(1000 + i);
			}
		}
	}

	public static String getInputName(int id) {
		if(id == 1100) return "Wheel Down";
		if(id == 1101) return "Wheel Up";
		if(id >= 1000) return Mouse.getButtonName(id - 1000);
		return Keyboard.getKeyName(id);
	}
	
	public static int nextInput() {
		int input;
		while(Keyboard.next()) {
			input = Keyboard.getEventKey();
			if(keyUnPress(input)) return input;
		}
		while(Mouse.next()) {
			input = Mouse.getEventButton();
			if(mouseUnPress(input)) return 1000 + input;
			input = Mouse.getEventDWheel();
			if(input != 0) {
				if(input < 0) {
					return 1100;
				} else {
					return 1101;
				}
			}
		}
		return -1;
	}
	
	public static boolean mouseDown(int id) {
		return Mouse.isButtonDown(id);
	}
	
	public static boolean mouseUp(int id) {
		return !Mouse.isButtonDown(id);
	}
	
	public static boolean mousePress(int id) {
		if(!down.containsKey(1000+id)) return false;
		boolean result = down.get(1000+id) ? true : false;
		down.put(1000+id, false);
		return result;
	}
	
	public static boolean mouseUnPress(int id) {
		if(!up.containsKey(1000+id)) return false;
		boolean result = up.get(1000+id) ? true : false;
		up.put(1000+id, false);
		return result;
	}
	
	public static boolean keyDown(int id) {
		return Keyboard.isKeyDown(id);
	}
	
	public static boolean keyUp(int id) {
		return !Keyboard.isKeyDown(id);
	}
	
	public static boolean keyPress(int id) {
		if(!down.containsKey(id)) return false;
		boolean result = down.get(id) ? true : false;
		down.put(id, false);
		return result;
	}
	
	public static boolean keyUnPress(int id) {
		if(!up.containsKey(id)) return false;
		boolean result = up.get(id) ? true : false;
		up.put(id, false);
		return result;
	}

	public static float getMouseWheel() {
		return mouseWheel;
	}
}
