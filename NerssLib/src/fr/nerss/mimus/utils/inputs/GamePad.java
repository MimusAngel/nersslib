package fr.nerss.mimus.utils.inputs;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;

public class GamePad {
	public final static int A 		= 0;
	public final static int B 		= 1;
	public final static int X 		= 2;
	public final static int Y 		= 3;
	public final static int LB 		= 4;
	public final static int RB		= 5;
	public final static int SELECT 	= 6;
	public final static int START 	= 7;
	public final static int LS 		= 8;
	public final static int RS 		= 9;

	public final static int CROSSX0	= 10;
	public final static int CROSSX1	= 11;
	public final static int CROSSY0 = 12;
	public final static int CROSSY1	= 13;
	public final static int LT 		= 14;
	public final static int RT 		= 15;
	
	private final static String inputsName[] = new String[] {
		"A", "B", "X", "Y", "LB", "RB", "Select", "Start", "LS", "RS", "Left", "Right", "Up", "Down", "LT", "RT"
	};
	
	private static Controller controller = null;
	
	private static ConcurrentHashMap<Integer, Boolean> buttonsDown = new ConcurrentHashMap<Integer, Boolean>();
	private static ConcurrentHashMap<Integer, Boolean> buttonsUp = new ConcurrentHashMap<Integer, Boolean>();
	
	private static ArrayList<Integer> gamepads = null;
	
	public static void create() {
		try {
			Controllers.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		Controllers.poll();
		for(int i = 0; i < Controllers.getControllerCount(); i++) {
			controller = Controllers.getController(i);
			if(controller.getName().toLowerCase().contains("xbox")) {
				if(gamepads == null) {
					gamepads = new ArrayList<Integer>();
				}
				gamepads.add(i);
			}
		}
		System.out.println("GamePad: initialized");
	}
	
	public static String getGamepadName(int id) {
		return Controllers.getController(gamepads.get(id)).getName();
	}
	
	public static Controller getGamepad(int id) {
		return Controllers.getController(gamepads.get(id));
	}
	
	public static void setController(Controller control) {
		controller = control;
	}
	
	public static boolean isUsed() {
		return controller != null;
	}
	
	public static void update() {
		if(controller == null) return;
		controller.poll();
		buttonsUp.clear();
		for(Entry<Integer, Boolean> button : buttonsDown.entrySet()) {
			if(buttonUp(button.getKey())) buttonsUp.put(button.getKey(), true);
			
		}
		
		buttonsDown.clear();
		for(int j = 0; j < 16; j++) {
			if(buttonDown(j)) buttonsDown.put(j, true);
		}
		
	}
	
	public static boolean buttonDown(int index) {
		if(controller == null) return false;
		if(index < controller.getButtonCount()) {
			return controller.isButtonPressed(index);
		}
		if(index == CROSSX0) return crossLeft();
		if(index == CROSSX1) return crossRight();
		if(index == CROSSY0) return crossUp();
		if(index == CROSSY1) return crossDown();
		if(index == LT) return triggerLeft();
		if(index == RT) return triggerRight();
		return false;
	}
	
	public static boolean buttonUp(int index) {
		return !buttonDown(index);
	}
	
	public static boolean buttonPress(int index) {
		if(!buttonsDown.containsKey(index)) return false;
		boolean result = buttonsDown.get(index);
		buttonsDown.put(index, false);
		return result;
	}
	
	public static boolean buttonUnPress(int index) {
		if(!buttonsUp.containsKey(index)) return false;
		boolean result = buttonsUp.get(index);
		buttonsUp.put(index, false);
		return result;
	}
	
	public static float getJoystickLeftX() {
		return controller.getXAxisValue();
	}
	
	public static float getJoystickLeftY() {
		return controller.getYAxisValue();
	}
	
	public static float getJoystickRightX() {
		return controller.getRXAxisValue();
	}
	
	public static float getJoystickRightY() {
		return controller.getRYAxisValue();
	}
	
	public static boolean crossLeft() {
		return controller.getPovX() < 0;
	}
	
	public static boolean crossRight() {
		return controller.getPovX() > 0;
	}

	public static boolean crossUp() {
		return controller.getPovY() < 0;
	}
	
	public static boolean crossDown() {
		return controller.getPovY() > 0;
	}
	
	public static boolean triggerLeft() {
		return trigger() > 0;
	}
	
	public static boolean triggerRight() {
		return trigger() < 0;
	}
	
	public static float trigger() {
		return controller.getZAxisValue();
	}
	
	public static boolean globalMoveLeft() {
		return crossLeft() || getJoystickLeftX() < 0;
	}
	
	public static boolean globalMoveRight() {
		return crossRight() || getJoystickLeftX() > 0;
	}
	
	public static boolean globalMoveUp() {
		return crossUp() || getJoystickLeftY() < 0;
	}
	
	public static boolean globalMoveDown() {
		return crossDown() || getJoystickLeftY() > 0;
	}
	
	public static int getGamepadCount() {
		if(gamepads == null) return 0;
		return gamepads.size();
	}
	
	public static void deadZoneModeGame() {
		if(controller == null) return;
		for(int j = 0; j < controller.getAxisCount(); j++) {
			controller.setDeadZone(j, 0.1f);
		}
	}
	
	public static void deadZoneModeGui() {
		if(controller == null) return;
		for(int j = 0; j < controller.getAxisCount(); j++) {
			controller.setDeadZone(j, 0.75f);
		}
	}
	
	public static int nextInput() {
		int id = -1;
		while(Controllers.next()) {
//			Controller c = Controllers.getController(Controllers.getEventControlIndex());
			if(Controllers.getEventSource() == controller) {
				if(Controllers.isEventButton()) {
					id = Controllers.getEventControlIndex();
					if(buttonUnPress(id)) return id;
				}
				if(Controllers.isEventPovX()) {
					if(buttonUnPress(CROSSX0)) return CROSSX0;
					if(buttonUnPress(CROSSX1)) return CROSSX1;
				}
				if(Controllers.isEventPovY()) {
					if(buttonUnPress(CROSSY0)) return CROSSY0;
					if(buttonUnPress(CROSSY1)) return CROSSY1;
				}
				if(Controllers.isEventAxis()) {
					if(buttonUnPress(LT)) return LT;
					if(buttonUnPress(RT)) return RT;
				}
			}
		}
		return -1;
	}

	public static String getInputName(int index) {
		return inputsName[index];
	}
}
