package fr.nerss.mimus.utils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;

import static org.lwjgl.opengl.GL11.*;

public class FrustumMimus {
	private static FloatBuffer screenCoords = BufferUtils.createFloatBuffer(3);
	private static IntBuffer viewport = BufferUtils.createIntBuffer(16);
	private static FloatBuffer modelView = BufferUtils.createFloatBuffer(16);
	private static FloatBuffer projection = BufferUtils.createFloatBuffer(16);
    
	public static void update() {
		screenCoords.clear();
	    modelView.clear();
	    projection.clear();
	    viewport.clear();

	    glGetFloat(GL_MODELVIEW_MATRIX,modelView);
	    glGetFloat(GL_PROJECTION_MATRIX,projection);
	    glGetInteger(GL_VIEWPORT,viewport);
	}
	
	public static int[] getScreenCoords(float x, float y, float z)
	{
	    boolean result = GLU.gluProject(x, y, z, modelView, projection, viewport, screenCoords);
	    if(result)
	    {
	        return new int[]
	        {
	            (int)screenCoords.get(0),
	            Display.getHeight() - (int) screenCoords.get(1)
	        };
	    }
	    return null;
	}

	public static boolean pointInScreen(float x, float y, float z) {
		int coord[] = getScreenCoords(x, y, z);
		if(coord != null) {
			if(coord[0] < 0 || coord[0] > Display.getWidth()
				|| coord[1] < 0 || coord[1] > Display.getHeight())
				return false;
			return true;
		}
		return false;
	}
	
//	static public Vector3f getMousePositionIn3dCoords(int mouseX, int mouseY)
//	   {
//
//	      viewport.clear();
//	      modelview.clear();
//	      projection.clear();
//	      winZ.clear();;
//	      position.clear();
//	      float winX, winY;
//
//
//	      GL11.glGetFloat( GL11.GL_MODELVIEW_MATRIX, modelview );
//	      GL11.glGetFloat( GL11.GL_PROJECTION_MATRIX, projection );
//	      GL11.glGetInteger( GL11.GL_VIEWPORT, viewport );
//
//	      winX = (float)mouseX;
//	      winY = /* (float)viewport.get(3) -  */  //Uncomment this if you invert Y
//	         (float)mouseY;
//
//	      GL11.glReadPixels(mouseX, (int)winY, 1, 1, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, winZ);
//
//	      float zz = winZ.get();
//
//	      GLU.gluUnProject(winX, winY, zz, modelview, projection, viewport, position);
//
//
//
//	      Vector3f v = new Vector3f (position.get(0),position.get(1),position.get(2));
//
//
//	      return v ;
//	   }
}
