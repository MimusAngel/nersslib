package fr.nerss.mimus.utils.maths;

public class Vector2i {
	public int x, y;

	public Vector2i() {
		this(0, 0);
	}
	
	public Vector2i(Vector2i v) {
		this(v.x, v.y);
	}
	public Vector2i(int v) {
		this(v, v);
	}
	public Vector2i(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Vector2i add(Vector2i v) {
		x += v.getX();
		y += v.getY();
		return this;
	}
	
	public Vector2i sub(Vector2i v) {
		x -= v.getX();
		y -= v.getY();
		return this;
	}
	
	public Vector2i mul(Vector2i v) {
		x *= v.getX();
		y *= v.getY();
		return this;
	}
	
	public Vector2i div(Vector2i v) {
		x /= v.getX();
		y /= v.getY();
		return this;
	}

	public Vector2i add(int v) {
		x += v;
		y += v;
		return this;
	}
	
	public Vector2i sub(int v) {
		x -= v;
		y -= v;
		return this;
	}
	
	public Vector2i mul(int v) {
		x *= v;
		y *= v;
		return this;
	}
	
	public Vector2i div(int v) {
		x /= v;
		y /= v;
		return this;
	}
	
	/*
	 * TODO : X
	 */
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public void addX(int x) {
		this.x += x;
	}

	/*
	 * TODO : Y
	 */
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void addY(int y) {
		this.y += y;
	}
	/*
	 * TODO: conv
	 */
	
	public int[] array() {
		return new int[]{x, y};
	}

	public String toString() {
		return "x: " + x + ", y: " + y;
	}
	
	public Vector2i copy() {
		return new Vector2i(this);
	}
	
	public boolean isNull() {
		return x == 0 && y == 0;
	}
	public Vector2i toMinAxis() {
		if(Math.abs(x) > Math.abs(y)) {
			x = 0;
		} else {
			y = 0;
		}
		return this;
	}
}
