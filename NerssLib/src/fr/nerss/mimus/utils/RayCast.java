package fr.nerss.mimus.utils;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;

import fr.nerss.mimus.render.Color4f;
import fr.nerss.mimus.utils.maths.Vector2f;
import fr.nerss.mimus.utils.maths.Vector3f;

public class RayCast {
	private float range;
	private float accuracy;
	private ArrayList<Vector3f> points;
	
	private int defaultPoint;
	
	public RayCast(float range, float accuracy) {
		this.range = range;
		this.accuracy = accuracy;
		defaultPoint = (int) (this.range / 3 * accuracy);
		points = new ArrayList<Vector3f>();
		for(int i = 0; i<range*accuracy; i++) {
			points.add(new Vector3f());
		}
	}

	public void update(Vector3f pos, Vector2f rot) {
		for(int i = 0; i<range*accuracy; i++) {
			float dist = (float)i * 1f/accuracy;
			points.set(i, pos.copy().add(eulerAngles(rot).mul(dist)));
		}
	}
	
	public void render(float size) {
		new Color4f(1f).bind();
		glPointSize(size);
		glBegin(GL_POINTS);
			for(int i = 0; i<range*accuracy; i++) {
				Vector3f v = points.get(i);
				glVertex3f(v.x, v.y, v.z);
			}
		glEnd();
		glPointSize(1f);
	}
	
	private Vector3f eulerAngles(Vector2f rotation) {
		Vector3f forward = new Vector3f();
		
		float cosY = (float) Math.cos(Math.toRadians(rotation.getY()-90));
		float sinY = (float) Math.sin(Math.toRadians(rotation.getY()-90));
		
		float cosX = (float) Math.cos(Math.toRadians(-rotation.getX()));
		float sinX = (float) Math.sin(Math.toRadians(-rotation.getX()));
		
		forward.setX(cosY * cosX);
		forward.setY(sinX);
		forward.setZ(sinY * cosX);
		
		forward.normalize();
		
		return forward;
	}

	public ArrayList<Vector3f> getPoints() {
		return points;
	}

	public float getRange() {
		return range;
	}

	public float getAccuracy() {
		return accuracy;
	}

	public int getDefaultPoint() {
		return defaultPoint;
	}

	public void setDefaultPoint(int defaultPoint) {
		this.defaultPoint = defaultPoint;
	}
}
