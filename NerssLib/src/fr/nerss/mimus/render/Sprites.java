package fr.nerss.mimus.render;

import java.io.IOException;

public class Sprites extends Texture {
	private float sizeX;
	private float sizeY;
	
	public Sprites(String path, int sx, int sy) throws IOException {
		super(path);
		this.sizeX = (float) sx / 512f;
		this.sizeY = (float) sy / (float) this.getHeight();
	}

	public QuadUV getUV(int x, int y) {
		return new QuadUV(this.sizeX * x, this.sizeX * x + this.sizeX,
				this.sizeY * y, this.sizeY * y + this.sizeY);
	}
}
