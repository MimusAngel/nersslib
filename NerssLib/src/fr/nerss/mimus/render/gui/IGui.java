package fr.nerss.mimus.render.gui;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import fr.nerss.mimus.render.Texture;
import fr.nerss.mimus.render.TrueTypeFont;
import fr.nerss.mimus.render.gui.event.IGuiEvent;
import fr.nerss.mimus.render.gui.event.IGuiEvent.IGuiEventStates;
import fr.nerss.mimus.render.gui.event.IGuiEventListener.IGuiEventUse;
import fr.nerss.mimus.utils.inputs.GamePad;

public abstract class IGui {
	public static TrueTypeFont font = new TrueTypeFont();
	public static TrueTypeFont fontTitle = new TrueTypeFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 18));
	
	protected ArrayList<IGuiEvent> events = new ArrayList<IGuiEvent>();
	
	protected int menuSelected = -1;
	
	public IGui addEvents(IGuiEvent... ev) {
		for(int i = 0; i < ev.length; i++) {
			events.add(ev[i]);
		}
		return this;
	}
	
	public IGui alignEventToColumn(int x, int y, int space, IGuiEvent... ev) {
		int offset = 0;
		for(int i = 0; i < ev.length; i++) {
			ev[i].setLocation(x, y + offset);
			offset += ev[i].getHeight() + space;
		}
		return this;
	}
	
	private void gamepadMenuNext(int value) {
		menuSelected += value;
		if(menuSelected < 0) menuSelected = events.size()-1;
		if(menuSelected >= events.size()) menuSelected = 0;
		while(!events.get(menuSelected).isEnable() || events.get(menuSelected).isLocked()) {
			menuSelected += value;
			if(menuSelected < 0) menuSelected = events.size()-1;
			if(menuSelected >= events.size()) menuSelected = 0;
		}
	}
	
	public void update(int tick) {
		if(GamePad.isUsed()) {
			if(tick % 7 == 0) {
				if(menuSelected == -1) {
					gamepadMenuNext(1);
				}
				if(GamePad.globalMoveUp()) {
					gamepadMenuNext(-1);
				}
				if(GamePad.globalMoveDown()) {
					gamepadMenuNext(1);
				}
			}
			
		} else {
			menuSelected = -1;
		}
		int mx = Mouse.getX();
		int my = Display.getHeight() - Mouse.getY();
		for(int i = 0; i < events.size(); i++) {
			if(events.get(i).isEnable() && !events.get(i).isLocked()) {
				events.get(i).update(mx, my);
				if(menuSelected == i) {
					events.get(i).gamepadUpdate(tick);
					if(GamePad.buttonUnPress(GamePad.A)) {
						events.get(i).use(IGuiEventUse.LEFT);
					}
					if(GamePad.buttonUnPress(GamePad.X)) {
						events.get(i).use(IGuiEventUse.RIGHT);
					}
					if(GamePad.buttonUnPress(GamePad.Y)) {
						events.get(i).use(IGuiEventUse.CENTER);
					}
					events.get(i).setStates(IGuiEventStates.GAMEPAD_SELECTED);
				}
			}
		}
	}
	
	public void render() {
		Texture.unbind();
		drawBackground();
		for(int i = 0; i < events.size(); i++) {
			if(events.get(i).isEnable()) events.get(i).render();
		}
		drawForeground();
	}

	public abstract void updateRezise();
	public abstract void drawBackground();
	public abstract void drawForeground();

	public boolean renderGuiOnly() {
		return true;
	}
	
}
