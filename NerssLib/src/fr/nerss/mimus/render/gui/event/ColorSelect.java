package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;
import fr.nerss.mimus.render.Color4f;

public class ColorSelect extends ISelect {

	int borderSize;
	Color4f color;
	
	public ColorSelect(int x, int y, int w, int h) {
		super(x, y, w, h);
		borderSize = 2;
		color = Color4f.WHITE.copy();
	}

	public void render() {
		glBegin(GL_QUADS);
			if(this.selected) 
				(new Color4f(0.3f)).add(Color4f.DARK_RED).bind();
			else
				(new Color4f(0.3f)).bind();
			
			glVertex2f(x, 			y);
			glVertex2f(x + width, 	y);
			glVertex2f(x + width, 	y + height);
			glVertex2f(x, 			y + height);
		
			if(this.selected) 
				(new Color4f(0.4f)).add(Color4f.DARK_RED).bind();
			else
				(new Color4f(0.4f)).bind();
			glVertex2f(x, 						y);
			glVertex2f(x + width - borderSize,	y);
			glVertex2f(x + width - borderSize, 	y + height - borderSize);
			glVertex2f(x, 						y + height - borderSize);
	
			if(this.selected) 
				(new Color4f(0.2f)).add(Color4f.DARK_RED).bind();
			else
				(new Color4f(0.2f)).bind();
			glVertex2f(x + borderSize, 	y + borderSize);
			glVertex2f(x + width,		y + borderSize);
			glVertex2f(x + width, 		y + height);
			glVertex2f(x + borderSize, 	y + height);
			
			color.bind();
			glVertex2f(x + borderSize, 			y + borderSize);
			glVertex2f(x + width - borderSize,	y + borderSize);
			glVertex2f(x + width - borderSize,  y + height - borderSize);
			glVertex2f(x + borderSize, 			y + height - borderSize);
		glEnd();
	}

	public void gamepadUpdate(int tick) {
		
	}

	public int getBorderSize() {
		return borderSize;
	}

	public void setBorderSize(int borderSize) {
		this.borderSize = borderSize;
	}

	public Color4f getColor() {
		return color;
	}

	public ColorSelect setColor(Color4f color) {
		this.color = color;
		return this;
	}


}
