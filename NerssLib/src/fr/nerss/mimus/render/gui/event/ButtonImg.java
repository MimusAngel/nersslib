package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.*;
import fr.nerss.mimus.render.Color4f;
import fr.nerss.mimus.render.Texture;

public class ButtonImg extends IGuiEvent{
	
	Texture texture;
	int borderSize;
	
	public ButtonImg(int x, int y, int w, int h, Texture tex) {
		this(x, y, w, h, tex, 2);
	}
	public ButtonImg(int x, int y, int w, int h, Texture tex, int bsize) {
		super(x, y, w, h);
		texture = tex;
		borderSize = bsize;
	}
	
	public void render() {
		if(this.locked) {
			glBegin(GL_QUADS);
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.1f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.5f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();	
			texture.bind();
			glBegin(GL_QUADS);
				(new Color4f(1f, 1f, 1f, 0.5f)).bind();
				glTexCoord2f(0, 0);
				glVertex2f(x + borderSize, 			y + borderSize);
				glTexCoord2f(1, 0);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glTexCoord2f(1, 1);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glTexCoord2f(0, 1);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();			
			return;
		}
		if(this.states == IGuiEventStates.MOUSE_DOWN) {
			glBegin(GL_QUADS);
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.4f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.6f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();
			
			texture.bind();
			glBegin(GL_QUADS);
				(new Color4f(1f, 1f, 1f, 0.5f)).bind();
				glTexCoord2f(0, 0);
				glVertex2f(x + borderSize, 			y + borderSize);
				glTexCoord2f(1, 0);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glTexCoord2f(1, 1);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glTexCoord2f(0, 1);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();	
		} else {
			float hover = 0;
			if(this.states == IGuiEventStates.HOVER || this.states == IGuiEventStates.GAMEPAD_SELECTED) {
				hover = 0.1f;
			}
			
			glBegin(GL_QUADS);
				(new Color4f(0.3f + hover)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.4f + hover)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.2f + hover)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				if(this.states == IGuiEventStates.GAMEPAD_SELECTED) {
					(new Color4f(0.6f + hover, 0.2f + hover, 0.2f + hover)).bind();
				} else {
					(new Color4f(0.6f + hover)).bind();
				}
				
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();
			texture.bind();
			glBegin(GL_QUADS);
				(new Color4f(1f, 1f, 1f, 1f - hover)).bind();
				glTexCoord2f(0, 0);
				glVertex2f(x + borderSize, 			y + borderSize);
				glTexCoord2f(1, 0);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glTexCoord2f(1, 1);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glTexCoord2f(0, 1);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();	
		}
		Texture.unbind();
	}
	
	public void gamepadUpdate(int tick) {}
}
