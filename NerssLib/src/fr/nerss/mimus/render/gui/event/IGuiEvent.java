package fr.nerss.mimus.render.gui.event;

import fr.nerss.mimus.render.TrueTypeFont;
import fr.nerss.mimus.render.gui.event.IGuiEventListener.IGuiEventUse;
import fr.nerss.mimus.utils.inputs.Inputs;

public abstract class IGuiEvent {
	public enum IGuiEventStates {
		NONE, HOVER, MOUSE_DOWN, GAMEPAD_SELECTED;
	}
	public static TrueTypeFont font = new TrueTypeFont();
	public static IGuiEvent focus = null;
	
	protected int x, y, width, height;
	protected IGuiEventListener listener = null;
	protected IGuiEventStates states = IGuiEventStates.NONE;
	protected boolean enable = true;
	protected boolean locked = false;
	
	public IGuiEvent(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}
	
	public void update(int mx, int my) {
		if(isHover(mx, my)) {
			if(Inputs.mouseDown(0) || Inputs.mouseDown(1) || Inputs.mouseDown(2)) {
				states = IGuiEventStates.MOUSE_DOWN;
				if(focus == null) focus = this;
			} else {
				states = IGuiEventStates.HOVER;
			}
			if(listener != null) {
				listener.mouseHover(this);
				if(Inputs.mouseUnPress(0)) {
					listener.mouseClick(IGuiEventUse.LEFT, this);
				}
				if(Inputs.mouseUnPress(1)) {
					listener.mouseClick(IGuiEventUse.RIGHT, this);
				}
				if(Inputs.mouseUnPress(2)) {
					listener.mouseClick(IGuiEventUse.CENTER, this);
				}
			}
		} else {
			states = IGuiEventStates.NONE;
			if(Inputs.mouseUp(0) && Inputs.mouseUp(1) && Inputs.mouseUp(2)) {
				focus = null;
			}
		}
	}
	
	public abstract void render();
	
	public boolean isHover(int mx, int my) {
		if(mx < x || mx >= x + width
			|| my < y || my >= y + height)
			return false;
		return true;
	}

	public IGuiEvent setListener(IGuiEventListener ev) {
		listener = ev;
		return this;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public int getX2() {
		return x + width;
	}
	
	public int getY2() {
		return y + height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public IGuiEventStates getStates() {
		return states;
	}

	public void setStates(IGuiEventStates states) {
		this.states = states;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	public void use(IGuiEventUse evType) {
		if(listener != null) listener.mouseClick(evType, this);
	}
	
	public abstract void gamepadUpdate(int tick);

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}
