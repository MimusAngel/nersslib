package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.*;
import fr.nerss.mimus.render.Color4f;
import fr.nerss.mimus.utils.inputs.GamePad;

public class HScrollbar extends IGuiEvent {
	private int borderSize = 1;
	private float value = 0;
	private boolean label = true;
	private int maxValue = 100;
	private int minValue = 0;
	
	public HScrollbar(int x, int y, int w, int h) {
		this(x, y, w, h, 0f);
	}
	
	public HScrollbar(int x, int y, int w, int h, float v) {
		super(x, y, w, h);
		value = v;
	}

	public void update(int mx, int my) {
		super.update(mx, my);
		if(focus == this) {
			float size = (float) width * 0.075f;
			float w = (float) this.width;
			value = ((mx-size/2) - x)/(w-size);
			if(value < 0) value = 0;
			if(value > 1) value = 1;
		}
	}
	
	public void render() {
		if(this.locked) {
			glBegin(GL_QUADS);
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.1f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
		
				(new Color4f(0.3f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.5f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
				
				float size = (float) width * 0.075f;
				float xx = x + value*(width-size);
	
				if(this.states == IGuiEventStates.GAMEPAD_SELECTED) {
					(new Color4f(0.6f, 0.3f, 0.3f)).bind();
				} else {
					(new Color4f(0.7f )).bind();
				}
				glVertex2f(xx, 			y);
				glVertex2f(xx + size, 	y);
				glVertex2f(xx + size, 	y + height);
				glVertex2f(xx, 			y + height);
			glEnd();
		} else {
			glBegin(GL_QUADS);
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
		
				(new Color4f(0.4f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.6f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
				
				float size = (float) width * 0.075f;
				float xx = x + value*(width-size);
	
				if(this.states == IGuiEventStates.GAMEPAD_SELECTED) {
					(new Color4f(0.7f, 0.3f, 0.3f)).bind();
				} else {
					(new Color4f(0.8f )).bind();
				}
				glVertex2f(xx, 			y);
				glVertex2f(xx + size, 	y);
				glVertex2f(xx + size, 	y + height);
				glVertex2f(xx, 			y + height);
			glEnd();
		}
		
		if(label) {
			String lbl = ""+getValue();
			float yy = y + height/2f - font.getHeight(lbl)/2f;
			font.drawString(lbl, getX2() + 5, yy);
		}
	}

	public int getBorderSize() {
		return borderSize;
	}

	public void setBorderSize(int borderSize) {
		this.borderSize = borderSize;
	}

	public int getValue() {
		return (int) Math.round(value*(maxValue-minValue))+minValue;
	}
	
	public void setValue(int value) {
		this.value = (float) (value-minValue) / (float) (maxValue-minValue);
	}
	
	public float getNormalizedValue() {
		return (float) value;
	}

	public void setNormalizedValue(float value) {
		this.value = value;
	}

	public boolean isLabel() {
		return label;
	}

	public void setLabel(boolean label) {
		this.label = label;
	}

	public void gamepadUpdate(int tick) {
		if(GamePad.isUsed() && tick % 5 == 0) {
			int v = this.getValue();
			if(GamePad.globalMoveRight()) {
				v++;
			}
			if(GamePad.globalMoveLeft()) {
				v--;
			}
			value = (float) v / (float) (maxValue-minValue);
			if(value < 0f) value = 0f;
			if(value > 1f) value = 1f;
		}
	}

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public int getMinValue() {
		return minValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}

}
