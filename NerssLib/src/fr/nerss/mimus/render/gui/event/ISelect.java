package fr.nerss.mimus.render.gui.event;

public abstract class ISelect extends IGuiEvent {
	
	protected boolean selected = false;
	
	public ISelect(int x, int y, int w, int h) {
		super(x, y, w, h);
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
}
