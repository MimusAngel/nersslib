package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.*;
import fr.nerss.mimus.render.Color4f;

public class Button extends Label {
	
	int borderSize;
	public Button(String txt, int x, int y, int w, int h) {
		this(txt, x, y, w, h, 2);
	}
	
	public Button(String txt, int x, int y, int w, int h, int bSize) {
		super(txt, x, y, w, h);
		borderSize = bSize;
	}
	
	public void render() {
		if(this.locked) {
			glBegin(GL_QUADS);
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.1f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.5f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();
		
			int tx = x + width / 2 - font.getWidth(txt) / 2;
			int ty = y + height / 2 - font.getHeight(txt) / 2;
			font.drawString(txt, tx, ty, txtColor.copy().sub(0.1f));
			
			return;
		}
		if(this.states == IGuiEventStates.MOUSE_DOWN) {
			glBegin(GL_QUADS);
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.4f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.6f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();
			
			int tx = x + width / 2 - font.getWidth(txt) / 2 + 1;
			int ty = y + height / 2 - font.getHeight(txt) / 2 + 1;
			font.drawString(txt, tx, ty, txtColor.copy().sub(0.1f));
		} else {
			float hover = 0;
			if(this.states == IGuiEventStates.HOVER || this.states == IGuiEventStates.GAMEPAD_SELECTED) {
				hover = 0.1f;
			}
			
			glBegin(GL_QUADS);
				(new Color4f(0.3f + hover)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.4f + hover)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
	
				(new Color4f(0.2f + hover)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				if(this.states == IGuiEventStates.GAMEPAD_SELECTED) {
					(new Color4f(0.6f + hover, 0.2f + hover, 0.2f + hover)).bind();
				} else {
					(new Color4f(0.6f + hover)).bind();
				}
				
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();

			int tx = x + width / 2 - font.getWidth(txt) / 2;
			int ty = y + height / 2 - font.getHeight(txt) / 2;
			font.drawString(txt, tx, ty, txtColor.copy().sub(hover/2f));
		}
	}
}
