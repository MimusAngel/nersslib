package fr.nerss.mimus.render.gui.event;

public class FloatInputBox extends InputBox {

	public FloatInputBox(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public boolean isValidChar(char character) {
		if(character == '.') return true;
		return Character.isDigit(character);
	}
}
