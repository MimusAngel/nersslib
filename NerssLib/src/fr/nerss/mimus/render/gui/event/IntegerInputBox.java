package fr.nerss.mimus.render.gui.event;

public class IntegerInputBox extends InputBox {

	public IntegerInputBox(int x, int y, int w, int h) {
		super(x, y, w, h);
	}
	
	public boolean isValidChar(char character) {
		return Character.isDigit(character);
	}
}
