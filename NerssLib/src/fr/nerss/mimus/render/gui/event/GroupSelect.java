package fr.nerss.mimus.render.gui.event;

import java.util.ArrayList;

public class GroupSelect extends IGuiEvent implements IGuiEventListener {

	protected ArrayList<ISelect> events;
	protected int currentSelect = 0;
	public GroupSelect() {
		super(0, 0, 0, 0);
		events = new ArrayList<ISelect>();
	}
	
	public GroupSelect addEvents(ISelect... ev) {
		for(int i = 0; i < ev.length; i++) {
			ev[i].setListener(this);
			events.add(ev[i]);
		}
		return this;
	}
	
	public void update(int mx, int my) {
		for(int i = 0; i < events.size(); i++) {
			if(events.get(i).isEnable() && !events.get(i).isLocked()) {
				events.get(i).update(mx, my);
			}
		}
	}
	
	public void render() {
		for(int i = 0; i < events.size(); i++) {
			if(events.get(i).isEnable()) events.get(i).render();
		}
	}

	public void gamepadUpdate(int tick) {
		
	}

	public void mouseClick(IGuiEventUse button, IGuiEvent src) {
		if(button == IGuiEventUse.LEFT) {
			for(int i = 0; i < events.size(); i++) {
				if(src == events.get(i)) {
					currentSelect = i;
					events.get(i).setSelected(true);
				} else {
					events.get(i).setSelected(false);
				}
			}
			if(this.listener != null) {
				this.listener.mouseClick(button, this);
			}
		}
	}

	public ISelect getSelectEvent() {
		return events.get(currentSelect);
	}
	
	public void mouseHover(IGuiEvent src) {
		
	}

	public ArrayList<ISelect> getEvents() {
		return events;
	}

	public int getCurrentSelect() {
		return currentSelect;
	}

	public void setCurrentSelect(int currentSelect) {
		this.currentSelect = currentSelect;
		for(int i = 0; i < events.size(); i++) {
			if(i == currentSelect) {
				events.get(i).setSelected(true);
			} else {
				events.get(i).setSelected(false);
			}
		}
	}

	public void setEvents(ArrayList<ISelect> events) {
		this.events = events;
	}

}
