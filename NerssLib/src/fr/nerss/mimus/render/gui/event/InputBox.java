package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import org.lwjgl.input.Keyboard;

import fr.nerss.mimus.render.Color4f;
import fr.nerss.mimus.render.gui.event.IGuiEventListener.IGuiEventUse;
import fr.nerss.mimus.utils.inputs.Inputs;

public class InputBox extends IGuiEvent {
	protected int borderSize = 1;
	protected String txt = "";
	protected boolean focus = false;
	protected int charMax = 0;
	
	public InputBox(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public void update(int mx, int my) {
		if(isHover(mx, my)) {
			if(Inputs.mouseDown(0) || Inputs.mouseDown(1) || Inputs.mouseDown(2)) {
				states = IGuiEventStates.MOUSE_DOWN;
			} else {
				states = IGuiEventStates.HOVER;
			}
			if(listener != null) {
				listener.mouseHover(this);
				if(Inputs.mouseUnPress(0)) {
					focus = true;
					listener.mouseClick(IGuiEventUse.LEFT, this);
					while (Keyboard.next());
				}
				if(Inputs.mouseUnPress(1)) {
					focus = true;
					listener.mouseClick(IGuiEventUse.RIGHT, this);
					while (Keyboard.next());
				}
				if(Inputs.mouseUnPress(2)) {
					focus = true;
					listener.mouseClick(IGuiEventUse.CENTER, this);
					while (Keyboard.next());
				}
			} else {
				if(Inputs.mouseUnPress(0) || Inputs.mouseUnPress(1) || Inputs.mouseUnPress(2)) {
					focus = true;
					while (Keyboard.next());
				}
			}
		} else {
			states = IGuiEventStates.NONE;
			if(Inputs.mouseDown(0) || Inputs.mouseDown(1) || Inputs.mouseDown(2)) {
				focus = false;
			}
		}
		
		boolean shift = false;
		if(focus) {
			while (Keyboard.next()) {
				shift = false;
				
				if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) {
					shift = !shift;
				}
				
	            if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
	            	focus = false;
	            } else if (Keyboard.isKeyDown(Keyboard.KEY_DELETE)) {
	            	txt = "";
	            } else if (Keyboard.isKeyDown(Keyboard.KEY_BACK) && Keyboard.getEventKeyState()) {
	            	if(txt.length() > 0) txt = txt.substring(0, txt.length() - 1);
	            } else if (Keyboard.getEventKeyState()) {
	            	char addChar = Keyboard.getEventCharacter();
	            	if(isValidChar(addChar) && (charMax == 0 || txt.length() <= charMax)) {
		                if (shift) {
		                    txt += Character.toUpperCase(addChar);
		                } else {
		                	txt += String.valueOf(addChar);
		                }
	            	}
	            }
	        }
		}
	}
	
	public boolean isValidChar(char character) {
		return Character.isLetterOrDigit(character);
	}
	
	public void render() {
		if(this.locked) {
			glBegin(GL_QUADS);
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.1f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
		
				(new Color4f(0.3f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.5f)).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
				
			glEnd();
		} else {
			glBegin(GL_QUADS);
				(new Color4f(0.3f)).bind();
				glVertex2f(x, 			y);
				glVertex2f(x + width, 	y);
				glVertex2f(x + width, 	y + height);
				glVertex2f(x, 			y + height);
			
				(new Color4f(0.2f)).bind();
				glVertex2f(x, 						y);
				glVertex2f(x + width - borderSize,	y);
				glVertex2f(x + width - borderSize, 	y + height - borderSize);
				glVertex2f(x, 						y + height - borderSize);
		
				(new Color4f(0.4f)).bind();
				glVertex2f(x + borderSize, 	y + borderSize);
				glVertex2f(x + width,		y + borderSize);
				glVertex2f(x + width, 		y + height);
				glVertex2f(x + borderSize, 	y + height);
				
				(new Color4f(0.6f + (focus ? 0.1f : 0f))).bind();
				glVertex2f(x + borderSize, 			y + borderSize);
				glVertex2f(x + width - borderSize,	y + borderSize);
				glVertex2f(x + width - borderSize,  y + height - borderSize);
				glVertex2f(x + borderSize, 			y + height - borderSize);
			glEnd();
		}
		
		int tx = x + borderSize + 2;
		int ty = y + height / 2 - font.getHeight(txt) / 2;
		String drawTxt = txt;
		while(font.getWidth(drawTxt) > width - borderSize * 2 - 2) {
			if(focus) {
				drawTxt = drawTxt.substring(1, drawTxt.length());
			} else {
				drawTxt = drawTxt.substring(0, drawTxt.length() - 1);
			}
		}
		font.drawString(drawTxt, tx, ty);
		
	}

	public void gamepadUpdate(int tick) { }

	public String getTxt() {
		return txt;
	}

	public void setTxt(String txt) {
		this.txt = txt;
	}

}
