package fr.nerss.mimus.render.gui.event;

public interface IGuiEventListener {
	public enum IGuiEventUse {
		LEFT, CENTER, RIGHT;
	}

	public void mouseClick(IGuiEventUse button, IGuiEvent src);
	public void mouseHover(IGuiEvent src);
}
