package fr.nerss.mimus.render.gui.event;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;
import fr.nerss.mimus.render.Color4f;

public class CheckBox extends ISelect {
	String label;
	public CheckBox(int x, int y, String label) {
		super(x, y, 10, 10);
		this.label = label;
		this.setHeight(font.getHeight(label));
		this.setWidth(font.getWidth(label) + 12);
	}
	
	public void render() {
		glBegin(GL_QUADS);
			int h = (this.getHeight() - 10) / 2;
			(new Color4f(0.3f)).bind();
			glVertex2f(x, 		y + h);
			glVertex2f(x + 10, 	y + h);
			glVertex2f(x + 10, 	y + h + 10);
			glVertex2f(x, 		y + h + 10);
		
			(new Color4f(0.2f)).bind();
			glVertex2f(x, 			y + h);
			glVertex2f(x + 10 - 1,	y + h);
			glVertex2f(x + 10 - 1, 	y + h + 10 - 1);
			glVertex2f(x, 			y + h + 10 - 1);
	
			(new Color4f(0.4f)).bind();
			glVertex2f(x + 1, 	y + h + 1);
			glVertex2f(x + 10,	y + h + 1);
			glVertex2f(x + 10, 	y + h + 10);
			glVertex2f(x + 1, 	y + h + 10);
			
			(new Color4f(0.6f)).bind();
			glVertex2f(x + 1, 		y + h + 1);
			glVertex2f(x + 10 - 1,	y + h + 1);
			glVertex2f(x + 10 - 1,  y + h + 10 - 1);
			glVertex2f(x + 1, 		y + h + 10 - 1);
			
			if(this.isSelected()) {
				(new Color4f(0.3f)).bind();
				glVertex2f(x + 3, 		y + h + 3);
				glVertex2f(x + 10 - 3,	y + h + 3);
				glVertex2f(x + 10 - 3,  y + h + 10 - 3);
				glVertex2f(x + 3, 		y + h + 10 - 3);
			}
		glEnd();
	
		font.drawString(label, x + 12, y);
	}
	
	public void gamepadUpdate(int tick) {}

}
