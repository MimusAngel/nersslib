package fr.nerss.mimus.render.gui.event;

import fr.nerss.mimus.render.Color4f;

public class Label extends IGuiEvent {
	
	protected String txt;
	protected Color4f txtColor;
	public Label(String txt, int x, int y) {
		this(txt, x, y, font.getHeight(txt), font.getWidth(txt));
	}
	
	public Label(String txt, int x, int y, int w, int h) {
		super(x, y, w, h);	
		this.txt = txt;
		txtColor = new Color4f(1);
	}

	public void render() {
		font.drawString(txt, x, y, txtColor);
	}

	public String getTxt() {
		return txt;
	}

	public void setTxt(String txt) {
		this.txt = txt;
	}

	public Color4f getTxtColor() {
		return txtColor;
	}

	public void setTxtColor(Color4f txtColor) {
		this.txtColor = txtColor;
	}

	public void gamepadUpdate(int tick) {}
}
