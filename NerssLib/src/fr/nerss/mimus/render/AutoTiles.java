package fr.nerss.mimus.render;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import fr.nerss.mimus.game.IMap;

public class AutoTiles extends Texture {

	float sizeX;
	float sizeY;
	public AutoTiles(String path) {
		super(load(path), true);
		sizeX = 32f / (float) this.getWidth();
		sizeY = 32f / (float) this.getHeight();
	}

	public AutoTiles(BufferedImage buff) {
		super(load(buff), true);
		sizeX = 32f / (float) this.getWidth();
		sizeY = 32f / (float) this.getHeight();
	}
	
	private static BufferedImage load(String path) {
		BufferedImage image = new BufferedImage(64, 96, BufferedImage.TYPE_INT_ARGB);
		Image ii = new ImageIcon(path).getImage();
		Graphics g = image.createGraphics();
			g.drawImage(ii, 0, 0, 64, 96, null);
		g.dispose();
		return load(image);
	}
	
	private static BufferedImage load(BufferedImage ii) {
		BufferedImage image = new BufferedImage(512, 512, BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.createGraphics();
			for(int i = 0; i <= 0xff; i ++) {
				int x = i % 16;
				int y = i / 16;
				int aa = i & 0x89;
				if(aa == 0x80 || aa == 0) {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 +16, 
							0 * 32, 1 * 32, 0 * 32 + 16, 1 * 32 + 16, null);
				} else if(aa == 0x09) {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 +16, 
							1 * 32, 0 * 32, 1 * 32 + 16, 0 * 32 + 16, null);
				} else if(aa == 0x01 || aa == 0x81) {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 +16, 
							0 * 32, 2 * 32, 0 * 32 + 16, 2 * 32 + 16, null);
				} else if(aa == 0x08 || aa == 0x88) {	
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 +16, 
							1 * 32, 1 * 32, 1 * 32 + 16, 1 * 32 + 16, null);
				} else {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 +16, 
							1 * 32, 2 * 32, 1 * 32 + 16, 2 * 32 + 16, null);
				}
				
				int ba = i & 0x13;
				if(ba == 0x10 || ba == 0) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							1 * 32 + 16, 1 * 32, 1 * 32 + 32, 1 * 32 + 16, null);
				} else if(ba == 0x03) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							1 * 32 + 16, 0 * 32, 1 * 32 + 32, 0 * 32 + 16, null);
				} else if(ba == 0x01 || ba == 0x11) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							1 * 32 + 16, 2 * 32, 1 * 32 + 32, 2 * 32 + 16, null);
				} else if(ba == 0x02 || ba == 0x12) {	
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							0 * 32 + 16, 1 * 32, 0 * 32 + 32, 1 * 32 + 16, null);
				} else {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							0 * 32 + 16, 2 * 32, 0 * 32 + 32, 2 * 32 + 16, null);
				}

				int bb = i & 0x26;
				if(bb == 0x20 || bb == 0) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							1 * 32 + 16, 2 * 32 + 16, 1 * 32 + 32, 2 * 32 + 32, null);
				} else if(bb == 0x06) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							1 * 32 + 16, 0 * 32 + 16, 1 * 32 + 32, 0 * 32 + 32, null);
				} else if(bb == 0x04 || bb == 0x24) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							1 * 32 + 16, 1 * 32 + 16, 1 * 32 + 32, 1 * 32 + 32, null);
				} else if(bb == 0x02 || bb == 0x22) {	
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							0 * 32 + 16, 2 * 32 + 16, 0 * 32 + 32, 2 * 32 + 32, null);
				} else {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							0 * 32 + 16, 1 * 32 + 16, 0 * 32 + 32, 1 * 32 + 32, null);
				}
				
				int ab = i & 0x4c;
				if(ab == 0x40 || ab == 0) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							0 * 32, 2 * 32 + 16, 0 * 32 + 16, 2 * 32 + 32, null);
				} else if(ab == 0x0c) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							1 * 32, 0 * 32 + 16, 1 * 32 + 16, 0 * 32 + 32, null);
				} else if(ab == 0x04 || ab == 0x44) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							0 * 32, 1 * 32 + 16, 0 * 32 + 16, 1 * 32 + 32, null);
				} else if(ab == 0x08 || ab == 0x48) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							1 * 32, 2 * 32 + 16, 1 * 32 + 16, 2 * 32 + 32, null);
				} else {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							1 * 32, 1 * 32 + 16, 1 * 32 + 16, 1 * 32 + 32, null);
				}
			}
		g.dispose();
		return image;
	}
	
	public QuadUV getUV(int b) {
		int x = b % 16;
		int y = b / 16;
		float u0 = (float) x * sizeX;
		float u1 = u0 + sizeX;
		float v0 = (float) y * sizeY;
		float v1 = v0 + sizeY;
		return new QuadUV(u0, u1, v0, v1);
	}
	
	public int getB(IMap map, int x, int y, int layer, int data) {
		return getB(map, x, y, layer, data, data);
	}

	public int getB(IMap map, int x, int y, int layer, int data, int defData) {
		int b = 0;
		
		if (map.getData(x, y - 1, layer, defData) == data) {
			b = b | 0x01;
		}
		if (map.getData(x + 1, y, layer, defData) == data) {
			b = b | 0x02;
		}
		if (map.getData(x, y + 1, layer, defData) == data) {
			b = b | 0x04;
		}
		if (map.getData(x - 1, y, layer, defData) == data) {
			b = b | 0x08;
		}
		if (map.getData(x + 1, y - 1, layer, defData) == data) {
			b = b | 0x10;
		}
		if (map.getData(x + 1, y + 1, layer, defData) == data) {
			b = b | 0x20;
		}
		if (map.getData(x - 1, y + 1, layer, defData) == data) {
			b = b | 0x40;
		}
		if (map.getData(x - 1, y - 1, layer, defData) == data) {
			b = b | 0x80;
		}
		return b;
	}
	
	public static boolean isAutoTiles(String path) {
		Image ii = new ImageIcon(path).getImage();
		return (ii.getHeight(null) == 96 && ii.getWidth(null) == 64);
	}
}
