package fr.nerss.mimus.render;

import java.io.IOException;

import fr.nerss.mimus.utils.io.DataBuffer;

public class ImageData extends Texture{
	public ImageData(String path) throws IOException {
		super(path);
	}
	
	public ImageData(int h, int w, int p[], boolean addCache) {
		super(h, w, p, addCache);
	}
	
	public void write(String path) {
		DataBuffer buffer = new DataBuffer(8 + pixels.length * 4);
		try {
			buffer.put(this.getWidth());
			buffer.put(this.getHeight());
			for(int i = 0; i < pixels.length; i++) {
				buffer.put(pixels[i]);
			}
			buffer.flip();
			buffer.write(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ImageData read(String path) {
		DataBuffer buffer = new DataBuffer();
		try {
			buffer.read(path);
			int width = buffer.getInt();
			int height = buffer.getInt();
			int pixels[] = new int[width*height];
			for(int i = 0; i < pixels.length; i++) {
				pixels[i] = buffer.getInt();
			}
			return new ImageData(width, height, pixels, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
