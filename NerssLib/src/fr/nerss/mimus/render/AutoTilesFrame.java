package fr.nerss.mimus.render;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class AutoTilesFrame {
	AutoTiles[] frames;
	
	public AutoTilesFrame(String path, int frame) {
		if(frame <= 0) frame = 1;
		frames = new AutoTiles[frame];
		Image ii = new ImageIcon(path).getImage();
		
		for(int i = 0; i < frame; i++) {
			BufferedImage image = new BufferedImage(64, 96, BufferedImage.TYPE_INT_ARGB);
			Graphics g = image.createGraphics();
				g.drawImage(ii, 0, 0, 64, 96, i * 64, 0, i * 64 + 64, 96, null);
			g.dispose();
			frames[i] = new AutoTiles(image);
		}
	}
	
	public AutoTiles getFrame(int i) {
		if(i < 0) i = 0;
		if(i >= size()) i = size() - 1;
		return frames[i];
	}
	
	public int size() {
		return frames.length;
	}
	
	
	public static boolean isAutoTilesAnim(String path) {
		Image ii = new ImageIcon(path).getImage();
		return (ii.getHeight(null) == 96 && ii.getWidth(null) > 64);
	}
}
