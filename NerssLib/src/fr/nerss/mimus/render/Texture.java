package fr.nerss.mimus.render;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

public class Texture{
	private static HashMap<String, Texture> caches = new HashMap<String, Texture>();
	
	private int id;
	private int height;
	private int width;
	int[] pixels;
	
	public Texture(String path) throws IOException {
		this(ImageIO.read(new FileInputStream(path)), false);
		caches.put(path, this);
	}
	
	public Texture(BufferedImage image, boolean addCache) {
		height = image.getHeight();
		width = image.getWidth();
		pixels = new int[width*height];
		image.getRGB(0, 0, width, height, pixels, 0, width);
		for(int i = 0; i < pixels.length; i++) {
			int a = (pixels[i] & 0xff000000) >> 24;
			int r = (pixels[i] & 0xff0000) >> 16;
			int g = (pixels[i] & 0xff00) >> 8;
			int b = (pixels[i] & 0xff);
			
			pixels[i] = a << 24 | b << 16 | g << 8 | r;
		}

		compile();
		
		if(addCache) {
			caches.put("NoName"+caches.size(), this);
		}
	}
	public Texture(int h, int w, int p[], boolean addCache) {
		height = h;
		width = w;
		pixels = p;

		compile();
		
		if(addCache) {
			caches.put("NoName"+caches.size(), this);
		}
	}
	
	public void compile() {
		id = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		IntBuffer buffer = BufferUtils.createIntBuffer(pixels.length);
		buffer.put(pixels);
		buffer.flip();
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		buffer.clear();
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, id);
	}
	
	public static void unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void dispose() {
		glDeleteTextures(id);
	}
	
	public static void clearCache() {
		for(Texture tex : caches.values()) {
			tex.dispose();
		}
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

}
