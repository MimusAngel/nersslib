package fr.nerss.mimus.render;


import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import fr.nerss.mimus.game.IMap;

public class SideTiles extends Texture {
	float sizeX;
	float sizeY;
	
	public SideTiles(String path) {
		super(load(path), true);
		sizeX = 32f / (float) this.getWidth();
		sizeY = 32f / (float) this.getHeight();
	}

	public SideTiles(BufferedImage buff) {
		super(load(buff), true);
		sizeX = 32f / (float) this.getWidth();
		sizeY = 32f / (float) this.getHeight();
	}
	
	private static BufferedImage load(String path) {
		BufferedImage image = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
		Image ii = new ImageIcon(path).getImage();
		Graphics g = image.createGraphics();
			g.drawImage(ii, 0, 0, 64, 64, null);
		g.dispose();
		return load(image);
	}
	
	private static BufferedImage load(BufferedImage ii) {
		BufferedImage image = new BufferedImage(512, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.createGraphics();
			for(int i = 0; i <= 0xf; i ++) {
				int x = i % 16;
				int y = 0;
				int aa = i & 0x5;
				if((aa & 0x4) == 0x4) {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 + 16, 
							0 * 32, 1 * 32, 0 * 32 + 16, 1 * 32 + 16, null);
				} else if((aa & 0x1) == 0x1) {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 + 16, 
							0 * 32, 0 * 32, 0 * 32 + 16, 0 * 32 + 16, null);
				} else {
					g.drawImage(ii, 
							x * 32, y * 32, x * 32 + 16, y * 32 + 16, 
							1 * 32, 0 * 32, 1 * 32 + 16, 0 * 32 + 16, null);
				}
				
				int ba = i & 0x6;
				if((ba & 0x4) == 0x4) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							0 * 32 + 16, 1 * 32, 0 * 32 + 32, 1 * 32 + 16, null);
				} else if((ba & 0x2) == 0x2) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							0 * 32 + 16, 0 * 32, 0 * 32 + 32, 0 * 32 + 16, null);
				} else {
					g.drawImage(ii, 
							x * 32 + 16, y * 32, x * 32 + 32, y * 32 + 16, 
							1 * 32 + 16, 0 * 32, 1 * 32 + 32, 0 * 32 + 16, null);
				}
				
				
				int bb = i & 0xa;
				if((bb & 0x8) == 0x8) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							0 * 32 + 16, 1 * 32 + 16, 0 * 32 + 32, 1 * 32 + 32, null);
				} else if((bb & 0x2) == 0x2) {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							0 * 32 + 16, 0 * 32 + 16, 0 * 32 + 32, 0 * 32 + 32, null);
				} else {
					g.drawImage(ii, 
							x * 32 + 16, y * 32 + 16, x * 32 + 32, y * 32 + 32, 
							1 * 32 + 16, 0 * 32 + 16, 1 * 32 + 32, 0 * 32 + 32, null);
				}
				
				
				int ab = i & 0x9;
				if((ab & 0x8) == 0x8) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							0 * 32, 1 * 32 + 16, 0 * 32 + 16, 1 * 32 + 32, null);
				} else if((ab & 0x1) == 0x1) {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							0 * 32, 0 * 32 + 16, 0 * 32 + 16, 0 * 32 + 32, null);
				} else {
					g.drawImage(ii, 
							x * 32, y * 32 + 16, x * 32 + 16, y * 32 + 32, 
							1 * 32, 0 * 32 + 16, 1 * 32 + 16, 0 * 32 + 32, null);
				}
			}
		g.dispose();
		return image;
	}
	
	public QuadUV getUV(int b) {
		int x = b % 16;
		float u0 = (float) x * sizeX;
		float u1 = u0 + sizeX;
		float v0 = 0;
		float v1 = 1;
		return new QuadUV(u0, u1, v0, v1);
	}
	
	public int getB(IMap map, int x, int y, int layer, int data) {
		return getB(map, x, y, layer, data, data);
	}
	
	public int getB(IMap map, int x, int y, int layer, int data, int defData) {
		int dataB = 0;
		if(map.getData(x - 1, y, layer, defData) == data) {
			dataB = dataB | 0x1;
		}
		if(map.getData(x + 1, y, layer, defData) == data) {
			dataB = dataB | 0x2;
		}
		if(map.getData(x, y - 1, layer, defData) == data) {
			dataB = dataB | 0x4;
		}
		if(map.getData(x, y + 1, layer, defData) == data) {
			dataB = dataB | 0x8;
		}

		return dataB;
	}
	
	public static boolean isSideTiles(String path) {
		Image ii = new ImageIcon(path).getImage();
		return (ii.getHeight(null) == 64 && ii.getWidth(null) == 64);
	}
}
