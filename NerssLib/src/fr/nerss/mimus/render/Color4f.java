package fr.nerss.mimus.render;

import org.lwjgl.opengl.GL11;

public class Color4f {
	public static Color4f WHITE = new Color4f(1f,1f,1f,1f);
	public static Color4f BLACK = new Color4f(0f,0f,0f,1f);
	public static Color4f GRAY = new Color4f(.5f,.5f,.5f,1f);
	public static Color4f DARK_GRAY = new Color4f(.25f,.25f,.25f,1f);
	public static Color4f LIGHT_GRAY = new Color4f(.75f,.75f,.75f,1f);
	public static Color4f RED = new Color4f(1f,0f,0f,1f);
	public static Color4f LIGHT_RED = new Color4f(1f,.5f,.5f,1f);
	public static Color4f DARK_RED = new Color4f(.25f,0f,0f,1f);
	public static Color4f GREEN = new Color4f(0f,1f,0f,1f);
	public static Color4f LIGHT_GREEN = new Color4f(.5f,1f,.5f,1f);
	public static Color4f DARK_GREEN = new Color4f(.5f,1f,0f,1f);
	public static Color4f BLUE = new Color4f(0f,0f,1f,1f);
	public static Color4f LIGHT_BLUE = new Color4f(.5f,.5f,1,1f);
	public static Color4f DARK_BLUE = new Color4f(0f,0f,.5f,1f);
	public static Color4f CYAN = new Color4f(0f,1f,1f,1f);
	public static Color4f LIGHT_CYAN = new Color4f(.5f,1f,1f,1f);
	public static Color4f DARK_CYAN = new Color4f(0f,.5f,.5f,1f);
	public static Color4f PINK = new Color4f(1f,0f,1f,1f);
	public static Color4f LIGHT_PINK = new Color4f(1f,.5f,1f,1f);
	public static Color4f DARK_PINK = new Color4f(.5f,0f,.5f,1f);
	public static Color4f YELLOW = new Color4f(1f,1f,0f,1f);
	public static Color4f LIGHT_YELLOW = new Color4f(1f,1f,.5f,1f);
	public static Color4f DARK_YELLOW = new Color4f(.5f,.5f,0f,1f);
	public static Color4f ORANGE = new Color4f(1f,.5f,0f,1f);
	public static Color4f LIGHT_ORANGE = new Color4f(1f,.75f,.5f,1f);
	public static Color4f DARK_ORANGE = new Color4f(.5f,.25f,0f,1f);
	public static Color4f PURPLE = new Color4f(.5f,0f,1f,1f);
	public static Color4f LIGHT_PURPLE = new Color4f(.75f,.5f,1f,1f);
	public static Color4f DARK_PURPLE = new Color4f(.25f,0f,.5f,1f);
	
	public float r, g, b, a;
	
	public Color4f(Color4f color) {
		this(color.r, color.g, color.b, color.a);
	}
	
	public Color4f(float gray) {
		this(gray, gray, gray, 1);
	}
	
	public Color4f(float r, float g, float b) {
		this(r, g, b, 1);
	}
	
	public Color4f(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public static Color4f getColor4f(int hexa) {
		return getColor4f((hexa & 0xff0000) >> 16, (hexa & 0xff00) >> 8, (hexa & 0xff), (hexa & 0xff000000) >> 24);
	}
	
	public static Color4f getColor4f(int r, int g, int b) {
		return getColor4f(r, g, b, 255);
	}
	
	public static Color4f getColor4f(int r, int g, int b, int a) {
		return new Color4f((float) r / 255f, (float) g / 255f, (float) b / 255f, (float) a / 255f);
	}
	
	public Color4f sub(float value) {
		this.r -= value;
		this.g -= value;
		this.b -= value;
		
		return this;
	}
	
	public Color4f sub(Color4f v) {
		r -= v.r;
		g -= v.g;
		b -= v.b;
		return this;
	}
	
	public Color4f add(float value) {
		this.r += value;
		this.g += value;
		this.b += value;
		
		return this;
	}
	
	public Color4f add(Color4f v) {
		r += v.r;
		g += v.g;
		b += v.b;
		return this;
	}
	
	public Color4f mul(float v) {
		r *= v;
		g *= v;
		b *= v;
		return this;
	}
	
	public Color4f mul(Color4f v) {
		r *= v.r;
		g *= v.g;
		b *= v.b;
		return this;
	}
	
	public Color4f div(float v) {
		r /= v;
		g /= v;
		b /= v;
		return this;
	}
	
	public Color4f div(Color4f v) {
		r /= v.r;
		g /= v.g;
		b /= v.b;
		return this;
	}
	
	public Color4f copy() {
		return new Color4f(r, g, b, a);
	}
	
	public void bind() {
		GL11.glColor4f(r, g, b, a);
	}
	
	public float[] array() {
		return new float[] {r, g, b, a};
	}
	
	public boolean equals(Color4f c) {
		return r == c.r && g == c.g && b == c.b && a == c.a;
	}
}
