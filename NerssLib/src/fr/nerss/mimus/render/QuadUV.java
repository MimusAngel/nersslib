package fr.nerss.mimus.render;

import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

public class QuadUV {
	float u0;
	float u1;
	float v0;
	float v1;
	
	public QuadUV() {
		this(0, 1, 0, 1);
	}
	
	public QuadUV(float u0, float u1, float v0, float v1) {
		this.u0 = u0;
		this.u1 = u1;
		this.v0 = v0;
		this.v1 = v1;
	}
	
	public void directRender(float x, float y, float w, float h) {
		float x0 = x;
		float y0 = y;
		float x1 = x + w;
		float y1 = y + h;
		glTexCoord2f(u0, v0); glVertex2f(x0, y0);
		glTexCoord2f(u1, v0); glVertex2f(x1, y0);
		glTexCoord2f(u1, v1); glVertex2f(x1, y1);
		glTexCoord2f(u0, v1); glVertex2f(x0, y1);
	}
}
