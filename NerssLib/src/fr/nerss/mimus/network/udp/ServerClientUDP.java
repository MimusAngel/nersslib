package fr.nerss.mimus.network.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import fr.nerss.mimus.network.IPacket;
import fr.nerss.mimus.network.IServer;
import fr.nerss.mimus.network.PacketManager;
import fr.nerss.mimus.utils.io.DataBuffer;

public class ServerClientUDP extends Thread{
	private int id;
	private InetAddress ip;
	private int port;
	private Queue<IPacket> packets;
	private DatagramSocket socket;
	private long timeout;
	private IServer server;
	
	public ServerClientUDP(int id, InetAddress ip, int port, IServer serv) {
		this.id = id;
		this.ip = ip;
		this.port = port;
		this.server = serv;
		this.socket = server.getSocket();
		packets = new ConcurrentLinkedQueue<IPacket>();
		timeout = System.currentTimeMillis();
	}
	
	public boolean addPacket(IPacket p) {
		return packets.offer(p);
	}
	
	public boolean is(String ip, int port) {
		return this.ip.getHostAddress().equalsIgnoreCase(ip) && this.port == port;
	}
	
	public boolean isConnected() {
		return (System.currentTimeMillis() - timeout) <= 5000;
	}
	
	public void run() {
		while(!packets.isEmpty()) {
			IPacket packet = packets.poll();
			packet.handleServer(this.server, this);
			timeout = System.currentTimeMillis();
		}
	}
	
	public void send(IPacket packet) {
		try {
			int packetID = PacketManager.getPacketID(packet.getClass());
			if(packetID < 0) return;
			DataBuffer buff = new DataBuffer();
			buff.put(packetID);
			packet.write(buff);
			buff.flip();
			DatagramPacket send = new DatagramPacket(buff.array(), buff.size(), ip, port);
			if(!socket.isClosed()) socket.send(send);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void closeRequest() {
		timeout = 0;
	}

	public int getIndex() {
		return id;
	}

	public void setIndex(int index) {
		this.id = index;
	}

	public InetAddress getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	public DatagramSocket getSocket() {
		return socket;
	}

	public long getTimeout() {
		return timeout;
	}

	public IServer getServer() {
		return server;
	}
}
