package fr.nerss.mimus.network.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import fr.nerss.mimus.network.IPacket;
import fr.nerss.mimus.network.IServer;
import fr.nerss.mimus.network.IServerEvent;
import fr.nerss.mimus.network.PacketManager;
import fr.nerss.mimus.utils.io.DataBuffer;

public class ServerUDP extends Thread implements IServer {
	private boolean running;
	protected DatagramSocket socket;

	private int maxClient;
	private ServerClientUDP clients[];
	
	private IServerEvent event = null;
	ServerUDP localInstance;
	
	public ServerUDP(int port, int maxClient) {
		this.maxClient = maxClient;
		running = false;
		System.out.println("Initialize Clients Socket.");
		clients = new ServerClientUDP[this.maxClient];
		try {
			socket = new DatagramSocket(port);
			System.out.println("Server listening... "+port);
			localInstance = this;
		} catch (SocketException e) {
			System.err.println("Server already listening... "+port);
			System.exit(-1);
		}
	}
	public void run() {
		running=true;
		if(event != null) {
			Thread gameLoop = new Thread() {
				public void run() {
					long lastTickTime = System.nanoTime();
					double tickTime = 1000000000f/60f;
					long lastTime = System.currentTimeMillis();
					int ticks = 0;
					while(running) {
						if(System.nanoTime()-lastTickTime>tickTime) {
							lastTickTime+=tickTime;
							for(int i = 0; i < clients.length; i++) {
								if(clients[i] == null) continue;
								if(!clients[i].isConnected()) {
									event.playerDisconnect(localInstance, i);
									clients[i] = null;
								} else {
									event.playerUpdate(localInstance, clients[i]);
								}
							}
							event.serverUpdate(localInstance, ticks);
							ticks++;
						}
						if(System.currentTimeMillis()-lastTime>1000) {
							lastTime+=1000;
							ticks = 0;
						}
					}
				}
			};
			gameLoop.start();
		}
		System.out.println("Server ready!");
		
		while(running) {
			DatagramPacket receive = new DatagramPacket(new byte[0xffff], 0xffff);
			try {
				socket.receive(receive);
				DataBuffer buff = DataBuffer.setData(receive.getData());
				int packetID = buff.getInt();
				if(PacketManager.IDUsed(packetID)) {
					IPacket packet = PacketManager.getPacketClass(packetID).newInstance();
					packet.read(buff);
					ServerClientUDP client = getClient(receive.getAddress(), receive.getPort());
					if(client == null) {
						client = newClient(receive.getAddress(), receive.getPort());
						if(event != null) event.playerConnect(this, client);
					}
					if(client != null) {
						client.addPacket(packet);
						if(client.getState() != Thread.State.RUNNABLE) {
							client.start();
						}
					} else {
						client = new ServerClientUDP(-1, receive.getAddress(), receive.getPort(), this);
						client.closeRequest();
						if(event != null) event.serverFull(this, client);
						client = null;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void close() {
		running = false;
	}
	
	private ServerClientUDP getClient(InetAddress ip, int port) {
		for(int i = 0; i < clients.length; i++) {
			if(clients[i] == null) continue;
			if(clients[i].is(ip.getHostAddress(), port)){
				if(!clients[i].isConnected()) continue;
				return clients[i];
			}
		}
		return null;
	}
	
	private ServerClientUDP newClient(InetAddress ip, int port) {
		int id = newClientID();
		if(id >= 0) {
			clients[id] = new ServerClientUDP(id, ip, port, this);
			return clients[id];
		}
		return null;
	}
	
	private int newClientID() {
		for(int i = 0; i < clients.length; i++) {
			if(clients[i] == null) return i;
			if(!clients[i].isConnected()) {
				if(event != null) event.playerDisconnect(this, i);
				return i;
			}
		}
		return -1;
	}
	
	public void sendTo(int id, IPacket packet) {
		if(clients[id] == null) return;
		if(!clients[id].isConnected()) return;
		clients[id].send(packet);
	}
	
	public void sendToAll(IPacket packet) {
		for(int i = 0; i < clients.length; i++) {
			sendTo(i, packet);
		}
	}

	public void sendToAny(int id, IPacket packet) {
		for(int i = 0; i < clients.length; i++) {
			if(i == id) continue;
			sendTo(i, packet);
		}
	}
	@Override
	public DatagramSocket getSocket() {
		return socket;
	}
	public IServerEvent getEvent() {
		return event;
	}
	public void setEvent(IServerEvent event) {
		this.event = event;
	}
}
