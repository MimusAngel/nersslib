package fr.nerss.mimus.network.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import fr.nerss.mimus.network.IPacket;
import fr.nerss.mimus.network.PacketManager;
import fr.nerss.mimus.utils.io.DataBuffer;

public class ClientUDP extends Thread{
	private InetAddress ip;
	private int port;
	private DatagramSocket socket;
	private long timeout;
	private boolean closed;
	
	public ClientUDP(InetAddress ip, int port) {
		this.ip = ip;
		this.port = port;
		try {
			this.socket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		timeout = System.currentTimeMillis();
	}
	
	public boolean is(String ip, int port) {
		return this.ip.getHostAddress().equalsIgnoreCase(ip) && this.port == port;
	}
	
	public boolean isConnected() {
		return (System.currentTimeMillis() - timeout) <= 5000;
	}
	
	public void run() {
		closed = false;
		ClientUDP local = this;
		while(!closed) {
			DatagramPacket receive = new DatagramPacket(new byte[0xffff], 0xffff);
			try {
				socket.receive(receive);
				DataBuffer buff = DataBuffer.setData(receive.getData());
				int packetID = buff.getInt();
				if(PacketManager.IDUsed(packetID)) {
					new Thread() {
						public void run() {
							IPacket packet;
							try {
								packet = PacketManager.getPacketClass(packetID).newInstance();
								packet.read(buff);
								packet.handleClient(local);
								timeout = System.currentTimeMillis();
							} catch (InstantiationException
									| IllegalAccessException e) {
								e.printStackTrace();
							}
						}
					}.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void closeRequest() {
		closed = true;
	}
	
	public void send(IPacket packet) {
		try {
			int packetID = PacketManager.getPacketID(packet.getClass());
			if(packetID < 0) return;
			DataBuffer buff = new DataBuffer();
			buff.put(packetID);
			packet.write(buff);
			buff.flip();
			DatagramPacket send = new DatagramPacket(buff.array(), buff.size(), ip, port);
			if(!socket.isClosed()) socket.send(send);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
