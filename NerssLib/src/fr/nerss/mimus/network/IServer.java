package fr.nerss.mimus.network;

import java.net.DatagramSocket;

public interface IServer {
	public void sendTo(int id, IPacket packet);
	public void sendToAll(IPacket packet);
	public void sendToAny(int id, IPacket packet);
	public DatagramSocket getSocket();
}
