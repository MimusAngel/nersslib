package fr.nerss.mimus.network;

import fr.nerss.mimus.network.udp.ServerClientUDP;

public interface IServerEvent {
	public void playerConnect(IServer serv, ServerClientUDP client);
	public void playerUpdate(IServer serv, ServerClientUDP client);
	public void playerDisconnect(IServer serv, int ID);
	public void serverFull(IServer serv, ServerClientUDP client);
	public void serverUpdate(IServer serv, int tick);
}
