package fr.nerss.mimus.network;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class PacketManager {
	private static Map<Integer, Class<? extends IPacket>> packets = new HashMap<Integer, Class<? extends IPacket>>();
	
	public static boolean IDUsed(int id) {
		return packets.containsKey(id);
	}
	
	public static int addPacket(Class<? extends IPacket> classPacket) {
		int newID = packets.size();
		packets.put(newID, classPacket);
		return newID;
	}
	
	public static Class<? extends IPacket> getPacketClass(int id) {
		return packets.get(id);
	}
	
	public static int getPacketID(Class<? extends IPacket> class1) {
		for(Entry<Integer, Class<? extends IPacket>> entry : packets.entrySet()) {
			if(entry.getValue() == class1) {
				return entry.getKey();
			}
		}
		return -1;
	}
}
