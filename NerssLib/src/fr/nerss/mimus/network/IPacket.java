package fr.nerss.mimus.network;

import fr.nerss.mimus.network.udp.ClientUDP;
import fr.nerss.mimus.network.udp.ServerClientUDP;
import fr.nerss.mimus.utils.io.DataBuffer;

public interface IPacket {
	public void write(DataBuffer buffer);
	public void read(DataBuffer buffer);
	public void handleServer(IServer serv, ServerClientUDP client);
	public void handleClient(ClientUDP client);
	
}
