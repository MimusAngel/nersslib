package fr.nerss.mimus.game;

public abstract class IMap {
	public abstract int getData(int x, int y, int layer, int def);
	public abstract boolean setData(int x, int y, int layer, int id);
}
