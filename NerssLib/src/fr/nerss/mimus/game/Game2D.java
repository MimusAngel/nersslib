package fr.nerss.mimus.game;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glViewport;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

import fr.nerss.mimus.render.DisplayUtils;
import fr.nerss.mimus.render.Texture;
import fr.nerss.mimus.render.TrueTypeFont;
import fr.nerss.mimus.render.gui.IGui;
import fr.nerss.mimus.utils.LWJGLNativeLoader;
import fr.nerss.mimus.utils.inputs.Inputs;

public abstract class Game2D {
	private static Game2D instance = null;
	public static Game2D getInstance() {
		return instance;
	}
	
	private boolean debug = false;
	private static String gameName ="";
	private static int gameVersion = 1;
	private static int gameSubVersion = 0;
	private static int gameRelease = 1;
	protected TrueTypeFont font;
	
	public Game2D(String title, int width, int height) {
		this(title, width, height, true);
	}
	
	public Game2D(String title, int width, int height, boolean resizable) {
		instance = this;
		LWJGLNativeLoader.addLWJGLNative("natives/");
		gameName = title;
		try {
			DisplayUtils.create(title, width, height, resizable);
			glEnable(GL_TEXTURE_2D);
			DisplayUtils.enableAlpha();
			font = new TrueTypeFont();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		init();
		loop();
	}
	
	private int ips = 60;
	private int ups = 60;
	private IGui gui = null;
	private void loop() {
		long lastTickTime = System.nanoTime();
		double tickTime = 1000000000f/60f;
		
		long lastTime = System.currentTimeMillis();
		
		int ticks = 0;
		int fps = 0;

		while(!Display.isCloseRequested()) {
			if(System.nanoTime()-lastTickTime>tickTime) {
				lastTickTime+=tickTime;
				Inputs.update();
				if(gui == null || !gui.renderGuiOnly()) {
					update(ticks);
				}
				if(gui != null) gui.update(ticks);
				ticks++;
			} else {
				glViewport(0, 0, Display.getWidth(), Display.getHeight());
				if(Display.wasResized() && gui != null) {
					gui.updateRezise();
				}
				
				DisplayUtils.glOrtho();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glColor3f(1,1,1);
				if(gui == null || !gui.renderGuiOnly()) {
					render();
				}
				if(gui != null) gui.render();
				
				if(debug) {
					debugRender();
				}
				
				Display.update();
				fps++;
			}
			
			if(System.currentTimeMillis()-lastTime>1000) {
				lastTime+=1000;
				ups=ticks;
				ips=fps;
				ticks = 0;
				fps = 0;
			}
		}
		destroy();
	}
	
	private void destroy() {
		dispose();
		Texture.clearCache();
		Display.destroy();
		System.exit(0);
	}
	
	public void stop() {
		
	}
	
	protected abstract void init();
	protected abstract void update(int tick);
	protected abstract void render();
	protected abstract void debugRender();
	protected abstract void dispose();

	public int getFPS() {
		return ips;
	}

	public int getUPS() {
		return ups;
	}

	public IGui getGui() {
		return gui;
	}

	public void setGui(IGui gui) {
		this.gui = gui;
	}
	
	public void noGui() {
		this.gui=null;
	}

	protected void setVersion(int v, int s, int r) {
		gameVersion=v;
		gameSubVersion=s;
		gameRelease=r;
	}
	
	public String getGameVersionInfo() {
		return getGameVersion()+"."+getGameSubVersion()+"r"+getGameRelease();
	}
	
	public String getGameName() {
		return gameName;
	}

	public int getGameVersion() {
		return gameVersion;
	}

	public int getGameSubVersion() {
		return gameSubVersion;
	}

	public int getGameRelease() {
		return gameRelease;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	public void setTitle(String title) {
		Display.setTitle(title);
	}
}
